package com.company;

public class Account {

    private long id;
    private String iban;
    private long sum;
    private Currency currency;

    public Account(String iban, Currency currency) {
        this.iban = iban;
        this.sum = 0;
        this.currency = currency;
    }

    public void addToSum(long money) {
        this.sum += money;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
