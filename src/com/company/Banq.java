package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Banq {

    private long id;
    private String name;
    private String country;
    private long money;

    private List<Client> clientList;

    public Banq(String name, String country, long money) {
        this.name = name;
        this.country = country;
        this.money = money;
        clientList = new ArrayList<>();
    }

    public void addClient(Client client) {
        clientList.add(client);
    }

    public void removeClient(Client client) {
        clientList.remove(client);
    }

    public void removeClient(Predicate<Client> toDelete) {
        clientList.removeIf(toDelete);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }
}
