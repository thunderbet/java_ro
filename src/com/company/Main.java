package com.company;


public class Main {

    public static void main(String[] args) {
        Client first = new Client("Bogdan", "Sabau", "RO 5165");
        Client second = new Client("Buzatu", "Mihai", "FR 5159");
        second.getAccount().setCurrency(Currency.EURO);
        second.getAccount().addToSum(100);

        second.setActive(false);

        Banq banca = new Banq("Banca Romaniei", "Romania", 5415425665L);

        banca.addClient(first);
        banca.addClient(second);

        banca.removeClient(first);

        //banca.removeClient(client -> client.isActive());

        System.out.println(banca);
    }

}
