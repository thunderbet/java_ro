package com.company;

public enum Currency {
    EURO,
    RON,
    DOLLAR,
    YEN;
}
