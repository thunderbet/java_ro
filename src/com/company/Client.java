package com.company;

import java.util.Objects;

/**
 * Client class
 */
public class Client {

    private long id;
    private String name;
    private String firstName;
    private boolean active;
    private String iban;
    private Account account;

    public Client(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
        this.active = true;
        this.iban = iban;
        this.account = new Account("", Currency.RON);
    }

    public Client(String name, String firstName, String iban) {
        this.name = name;
        this.firstName = firstName;
        this.active = true;
        this.iban = iban;
        this.account = new Account(iban, Currency.RON);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return name.equals(client.name) &&
                firstName.equals(client.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, firstName);
    }
}
